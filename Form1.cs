﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Xml;
using Ionic.Zip;
using System.IO;
using System.Diagnostics;
using System.Net;

namespace WinApps
{
    public partial class Form1 : Form
    {
        Dictionary<string, catagory_index> Catagories = new Dictionary<string, catagory_index>();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MessageBox.Show("This Software was designed for installing FOSS and abandonware. Piracy of modern software is highly discouraged by the developer. Tifa Software will NOT be held responsible for your actions.",
                "Disclaimer"
                );

            //if (File.Exists("rapps.zip"))
            //{
                string[] CatagoryNames = {"Sound",
                                         "Video",
                                         "Graphics",
                                         "Games and Entertainment",
                                         "Internet and Networks",
                                         "Office stuff",
                                         "Development/programming",
                                         "Education",
                                         "Engineering",
                                         "Financial",
                                         "Science",
                                         "Tools",
                                         "Drivers",
                                         "Libraries",
                                         "Themes",
                                         "Other"};

                var rost = treeView1.Nodes.Add("Official Listings");
                int countros = 0;
                foreach (catagory_index cat in ReactOSRepository.GetRepository())
                {
                    var catnode = rost.Nodes.Add(CatagoryNames[countros]);
                    Catagories.Add(catnode.FullPath, cat);
                    countros++;
                }
            //}

            if (Properties.Settings.Default.Repositories != null)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(index));
                    XmlSerializer serializer2 = new XmlSerializer(typeof(catagory_index));
                    WebClient wc = new WebClient();
                    int count = 0;
                    foreach (string repo in Properties.Settings.Default.Repositories)
                    {
                        try
                        {
                            wc.DownloadFile(repo + "/repo_manifest.trf", "manifest" + count + ".xml");
                            var read = new XmlTextReader("manifest" + count + ".xml");
                            index resultingIndex = (index)serializer.Deserialize(read);
                            TreeNode top = treeView1.Nodes.Add(resultingIndex.name);

                            int count2 = 0;
                            foreach (indexCatagory cat in resultingIndex.catagory)
                            {
                                try
                                {
                                    var cn = top.Nodes.Add(cat.name); ;
                                    if (cat.file.StartsWith("https://") || cat.file.StartsWith("http://"))
                                        wc.DownloadFile(cat.file, "category" + count + "-" + count2 + ".xml");
                                    else
                                        wc.DownloadFile(repo + "/" + cat.file, "category" + count + "-" + count2 + ".xml");
                                    var read2 = new XmlTextReader("category" + count + "-" + count2 + ".xml");
                                    Catagories.Add(cn.FullPath, (catagory_index)serializer2.Deserialize(read2));
                                    read2.Close();
                                    File.Delete("category" + count + "-" + count2 + ".xml");
                                    count2++;
                                }
                                catch {
                                    MessageBox.Show("Unable to proccess Catagory File.");
                                }

                            }
                            read.Close();
                            File.Delete("manifest" + count + ".xml");
                            count++;
                        }
                        catch (Exception ee){
                            MessageBox.Show("Unable to Access Manifest" + "\nInfo: "+ee.ToString());
                        }
                    }
                }
                catch (Exception ee)
                {
                    MessageBox.Show("Unable to access repositories.", "Repository Error");
                }
            }
            else {
                //MessageBox.Show("You will need to provide urls for repositories.", "First Run"); 
           }
            toolStripStatusLabel1.Text = "Ready";
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About a = new About();
            a.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void repositoriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RepoConfig rc = new RepoConfig();
            rc.ShowDialog();
        }

        private void ResetBox() {
            button1.Enabled = false;
            textBox1.Text = "To install a program, selected a catagory under a repository in the left panel, and select the desired application above.";
            label1.Text = "No Software Selected";
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            ResetBox();
            listView1.Items.Clear();
            try
            {
                if (Catagories.ContainsKey(e.Node.FullPath))
                {
                    foreach (catagory_indexProgram p in Catagories[e.Node.FullPath].Items)
                    {
                        var l = listView1.Items.Add(p.name);
                        l.SubItems.Add(p.sd);
                        l.SubItems.Add(p.license);
                    }
                }
            }
            catch { 
                
            }
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            button1_Click(this, new EventArgs());
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                var item = Catagories[treeView1.SelectedNode.FullPath].Items[listView1.SelectedItems[0].Index];
                label1.Text = item.name;
                textBox1.Text = item.description.Trim();
                button1.Enabled = true;
            }
            else
            {
                ResetBox();
            }
        }

        bool isExeInstead = false;
        string fileExtension = ".exe";
        private void button1_Click(object sender, EventArgs e)
        {
            isExeInstead = false;
            var item = Catagories[treeView1.SelectedNode.FullPath].Items[listView1.SelectedItems[0].Index];
            WebClient wc = new WebClient();

            wc.DownloadProgressChanged += new DownloadProgressChangedEventHandler(wc_DownloadProgressChanged);
            wc.DownloadFileCompleted += new AsyncCompletedEventHandler(wc_DownloadFileCompleted);
            try {
            if (item.zipfile == "" || item.zipfile == null)
            {
                fileExtension = Path.GetExtension(item.exefile);
                try
                {
                    wc.DownloadFileAsync(new Uri(item.exefile), "tmp" + fileExtension);
                }
                catch
                {
                    MessageBox.Show("The Current SSL stuff doesn't support XP, and the repository chose that link for some stupid reason. We put the download link in the clipboard though.");
                    Clipboard.SetText(item.exefile);
                }
                isExeInstead = true;
            }
            else
                wc.DownloadFileAsync(new Uri(item.zipfile), "pkg.zip");
            }
            catch {
                MessageBox.Show("Could not download package.");
            }

        }
        ZipFile zfU;
        void wc_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (isExeInstead) {
                try
                {
                    Process p = Process.Start("tmp" + fileExtension);
                }
                catch {
                    MessageBox.Show("Invalid Program Installed");
                }
            }
            else
            {
                var item = Catagories[treeView1.SelectedNode.FullPath].Items[listView1.SelectedItems[0].Index];
                ZipFile zf = new ZipFile("pkg.zip");
                zfU = zf;
                zf.ExtractProgress += new EventHandler<ExtractProgressEventArgs>(zf_ExtractProgress);
                zf.Password = item.zippassword;
                zf.ExtractAll("\\tmp\\", ExtractExistingFileAction.OverwriteSilently);
            }
            
        }

        void zf_ExtractProgress(object sender, ExtractProgressEventArgs e)
        {
            toolStripProgressBar1.Value = (int)((float)e.BytesTransferred / (float)e.TotalBytesToTransfer) * 100;
            if (e.BytesTransferred == e.TotalBytesToTransfer && e.EventType == ZipProgressEventType.Extracting_AfterExtractAll)
            {
                var item = Catagories[treeView1.SelectedNode.FullPath].Items[listView1.SelectedItems[0].Index];
                try
                {
                    zfU.Dispose();
                    File.Delete("pkg.zip");
                    Process p = Process.Start("\\tmp\\" + item.exefile, item.exeargs);
                    //p.WaitForExit();
                    
                    //
                }
                catch { }
            }
        }

        void wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            toolStripProgressBar1.Value = e.ProgressPercentage;
        }

        private void listView1_Click(object sender, EventArgs e)
        {
            
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void reactOSRepositoryToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cleanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Directory.Exists("\\tmp\\"))
            {
                Directory.Delete("\\tmp\\", true);
            }
        }
    }
}