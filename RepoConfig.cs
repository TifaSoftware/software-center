﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinApps
{
    public partial class RepoConfig : Form
    {
        public RepoConfig()
        {
            InitializeComponent();
        }

        private void RepoConfig_Load(object sender, EventArgs e)
        {
            try
            {
                foreach (string x in Properties.Settings.Default.Repositories)
                {
                    listBox1.Items.Add(x);
                }
            }
            catch {
                Properties.Settings.Default.Repositories = new System.Collections.Specialized.StringCollection();
                Properties.Settings.Default.Save();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Repositories.Clear();
            foreach (string x in listBox1.Items)
            {
                
                Properties.Settings.Default.Repositories.Add(x);
                
            }
            Properties.Settings.Default.Save();
            MessageBox.Show("Please Restart Application for changes to occur.", "Repository Configuration");
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var url = textBox1.Text;
            if (url.EndsWith("/"))
                url = url.Substring(0, url.Length - 1);
            if (url.StartsWith("http://") || url.StartsWith("https://") || listBox1.Items.Contains(url) == false)
                listBox1.Items.Add(url);
            else if (listBox1.Items.Contains(url))
                MessageBox.Show("Url is already added", "Repository Configuration");
            else
                MessageBox.Show("Invalid Url", "Repository Configuration");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox1.Items.Remove(listBox1.SelectedItem);
        }
    }
}
