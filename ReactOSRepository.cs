﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using Ionic.Zip;
using System.IO;
using System.Windows.Forms;

namespace WinApps
{
    public class ReactOSRepository
    {
        public static catagory_index[] GetRepository() 
        {
            List<catagory_index> i = new List<catagory_index>();
            WebClient wc = new WebClient();
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;
            //wc.DownloadFile("https://github.com/reactos/rapps-db/archive/master.zip", "rapps.zip");
            //Add in pre-defined catagories
            var c1 = new catagory_index(); // Audio
            var c2 = new catagory_index(); // Video 
            var c3 = new catagory_index(); // Graphics 
            var c4 = new catagory_index(); // Games and Entertainment
            var c5 = new catagory_index(); // Internet and Networks 
            var c6 = new catagory_index(); // Office stuff 
            var c7 = new catagory_index(); // Development/programming
            var c8 = new catagory_index(); // Education  
            var c9 = new catagory_index(); // Engineering   
            var c10 = new catagory_index(); //  Financial  
            var c11 = new catagory_index(); // Science
            var c12 = new catagory_index(); // Tools 
            var c13 = new catagory_index(); // Drivers 
            var c14 = new catagory_index(); // Libraries 
            var c15 = new catagory_index(); // Themes 
            var c16 = new catagory_index(); // Other
            i.Add(c1);
            i.Add(c2);
            i.Add(c3);
            i.Add(c4);
            i.Add(c5);
            i.Add(c6);
            i.Add(c7);
            i.Add(c8);
            i.Add(c9);
            i.Add(c10);
            i.Add(c11);
            i.Add(c12);
            i.Add(c13);
            i.Add(c14);
            i.Add(c15);
            i.Add(c16);

            //MessageBox.Show(Directory.GetFiles(".\\rapps\\").Count().ToString());
            foreach (var fs in Directory.GetFiles(".\\rapps\\")) 
            {
                var f = File.OpenRead(fs);
                Dictionary<string, string> vs = new Dictionary<string, string>();
                var prog = new catagory_indexProgram();
                var rf = new StreamReader(f);
                string qualify = rf.ReadLine().Trim();
                //MessageBox.Show(qualify);
                    if (qualify == "[Section]")
                    {
                        //MessageBox.Show("Mark");
                        while (rf.EndOfStream == false)
                        {
                            string x = rf.ReadLine();
                            if (x.Contains('='))
                            {
                                var xx = x.Split('=');
                                if (vs.ContainsKey(xx[0].Trim()) == false)
                                {
                                    vs.Add(xx[0].Trim(), xx[1].Trim());
                                }
                            }
                        }

                        prog.description = ConditionalAdd(vs, "Description") + "\n Version: " + ConditionalAdd(vs, "Version");
                        prog.exefile = ConditionalAdd(vs, "URLDownload");
                        prog.license = ConditionalAdd(vs, "License");
                        prog.name = ConditionalAdd(vs, "Name");
                        //MessageBox.Show(prog.name);
                        prog.sd = ConditionalAdd(vs, "Description");
                        List<catagory_indexProgram> tmp;
                        if (i[int.Parse(vs["Category"]) - 1].Items == null)
                        {
                            tmp = new List<catagory_indexProgram>();
                        } else 
                        {
                            tmp = i[int.Parse(vs["Category"]) - 1].Items.ToList(); ;
                        }
                        tmp.Add(prog);
                        i[int.Parse(vs["Category"]) - 1].Items = tmp.ToArray();
                    }
                }
            
            return i.ToArray();
        }

        public static string ConditionalAdd (Dictionary<string, string> d, string key) 
        {
            if (d.ContainsKey(key))
                return d[key];
            else
                return "";
        }
    }
}
