# Software Center #

* This Program downloads and installs software with the use of a web-browser.
* Version 0.9 Beta

### Building Requirements ###

* .Net Framework 4
* Visual Studio 2010

### Contribution ###

* Any improvements are encouraged, however, we reccomend that you use Visual Studio 2010 because we want this program to be buildable in Windows XP/Server 2003.

### Who do I talk to? ###

* Inquires go to tifasoftware@gmx.com