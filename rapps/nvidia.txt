﻿[Section]
Name = NVIDIA Graphics Card Drivers
Version = 368.81
License = Free
Description = Drivers for GeForce Graphics card all up until the GTX 1000's.
Category = 13
URLDownload = https://i430vx.net/files/XP/EOL/368.81-desktop-winxp-32bit-international.exe